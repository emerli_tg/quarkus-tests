package org.acme;

import io.quarkus.panache.common.Sort;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("/")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
public class GreetingResource {
    @ConfigProperty(name = "greeting.message")
    String message;
    IResource resource;

    private static final Logger LOG = Logger.getLogger(GreetingResource.class);

    public GreetingResource(IResource resource) {
        this.resource = resource;

    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        LOG.error("aaaaaaaaa");

        return "Hello " + message;
    }


    @GET
    @Path("/list")
    public List<User> list() {
        return User.listAll(Sort.by("username"));
    }

    @GET
    @Path("/get-name/{id}")
    public String getName(Long id) {
        Optional<User>  u = User.findByIdOptional(id);
        return u.map(c->c.username).orElse("EMPTY");

    }


//
//
//    @Path("/list/{office}/{role}")
//    @GET
//    public Response list(String office, String role) {
//        Long c = User.countByOfficeAndRole(office, role);
//        List<User> d = User.findByOfficeAndRole(office, role);
//        return Response.ok(new Results<>(c, d)).build();
//    }
//
//
//    @Path("/test/{name}/{age:\\d+}")
//    @GET
//    public String personalisedHello(String name, int age) {
//        return "Hello " + name + " is your age really " + age + "?";
//
//
//    }
}