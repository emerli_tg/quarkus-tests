package org.acme;
        import io.quarkus.hibernate.orm.panache.PanacheEntity;
        import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

        import javax.persistence.*;
        import java.util.Date;
        import java.util.List;

@Entity
@Table(name = "resources")
public class User extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_resource")
    public Long idResource;
    @Column(name = "resource_type_id")
    public String resourceTypeId;
    @Column(name = "client_rule_id")
    public String resourceClientRuleId;
    @Column(name = "office_id")
    public String office_id;
    @Column(name = "role_id")
    public String role_id;

    @Column(name = "username")
    public String username;
    @Column(name = "password_hash")
    public String passwordHash;
    @Column(name = "password_salt")
    public String passwordSalt;
    @Column(name = "password")
    public String password;
    @Column(name = "company_name")
    public String companyName;
    @Column(name = "website")
    public String website;
//    @Column(name = "FTP_bs_address")
//    public String ftpBsAddress;
//    @Column(name = "FTP_bs_account")
//    public String ftpBsAccount;
//    @Column(name = "FTP_bs_password")
//    public String ftpBsPassword;
//    @Column(name = "FTP_pers_address")
//    public String ftpPersAddress;
//    @Column(name = "FTP_pers_account")
//    public String ftpPersAccount;
//    @Column(name = "FTP_pers_password")
//    public String ftpPersPassword;
//    @Column(name = "tax_id")
//    public String taxId;
//    @Column(name = "VAT")
//    public String vat;
//    @Column(name = "ext_id")
//    public String extId;
//    @Column(name = "is_enable")
//    public Boolean isEnabled;
//    @Column(name = "profile_mnemo")
//    public String profileMnemo;
//    @Column(name = "cq_role")
//    public String cqRole;
//    @Column(name = "state_mnemo")
//    public String stateMnemo;
//    @Column(name = "Imported_id")
//    public String importedId;
//    @Transient
//    public String passwordTmp;
    // Classification
    @Column(name = "from_date")
    public Date from;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "teamwork_id")
//    public GenericCombo teamworkId;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "channel_id")
//    public GenericCombo resourceCategory;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "site_id")
//    public GenericCombo site;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "ref_pers_id")
//    @JsonSerialize(using = ResourceSerializer.class)
//    public it.bs.vms.backend.model.Resource refPers;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "hierarchy_id")
//    @JsonSerialize(using = ResourceSerializer.class)
//    public it.bs.vms.backend.model.Resource hierarchy;
//    @Column(name = "note")
//    public String note;
//    @Column(name = "sent_availability_date")
//    public Date sentAvailabilityDate;
////    @ManyToOne(fetch = FetchType.LAZY)
////    @JoinColumn(name = "timezone_id")
////    public GenericCombo timezoneId;
//    @Column(name = "company_name_code")
//    public String companyNameCode;
//    @Column(name = "password_type")
//    public Integer passwordType;
//    @Column(name = "password_change_date")
//    public Date passwordChangeDate;
//    @Column(name = "password_error_times")
//    public Integer passwordErrorTimes;
//    @Column(name = "password_error_time_date")
//    public Date passwordErrorTimesDate;
//    @Column(name = "tax_residence")
//    public String taxResidence;
//    @Column(name = "country_payment")
//    public String countryPayment;
//    @Column(name = "email")
//    public String email;
//    @Column(name = "telephone_number")
//    public String telephoneNumber;
//    @Column(name = "fax_number")
//    public String faxNumber;
//    @Column(name = "note_wh")
//    public String noteWh;
//    @Column(name = "is_ref_person")
//    public Boolean isRefPerson;
//    @Column(name = "login")
//    public Date login;
//    @Column(name = "logout")
//    public Date logout;
//    @Column(name = "is_active")
//    public Boolean isActive;
//    @Column(name = "invoicing_note")
//    public String invoicingNote;
////    @ManyToOne(fetch = FetchType.LAZY)
////    @JoinColumn(name = "invoicing_type")
////    public GenericCombo invoicingType;
////    @Column(name = "paypal_account")
////    public String paypalAccount;
////    @Column(name = "to_be_tracked")
////    @Convert(converter = BooleanToStringConverter.class)
//    public Boolean toBeTracked;
//    @Column(name = "memo")
//    public String memo;
//    @Column(name = "utc_timezone_id")
//    public String utcTimezoneId;
//    @Column(name = "is_confirmed")
//    public Boolean isConfirmed;
//    @Column(name = "is_deleted")
//    public Boolean isDeleted;
//    @Column(name = "code_ape")
//    public String codeApe;
//    @Column(name = "agessa")
//    public String agessa;
//    @Column(name = "securite_sociale")
//    public String securiteSociale;
//    @Column(name = "deleted_date")
//    public Date deletedDate;
//    @Column(name = "is_resource_manager")
//    public Boolean isResourceManager;
//    @Column(name = "buyout_enabled")
//    public Boolean buyoutEnabled;
//    @Column(name = "individual_company")
//    public Boolean individualCompany = false;
////    @OneToOne(fetch = FetchType.LAZY)
////    @JoinColumn(name = "deleted_by")
////    public it.bs.vms.backend.model.Resource deletedBy;
////    @Column(name = "recording_studio")
////    public Boolean recordingStudio = false;
////
////    @OneToOne(cascade = CascadeType.ALL)
////    @JoinColumn(name = "new_role_id", referencedColumnName = "id")
////    public Role role;

    public static List<User> findByOfficeAndRole(String office, String role){

        return find("office_id=?1 and role_id=?2 ",office,role).list();
    }

    public static Long countByOfficeAndRole(String office, String role){
        return count("office_id=?1 and role_id=?2 ",office,role);
    }
}