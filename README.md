# QuarkusTests
Projects of experiments with Quarkus

## How To Get Started
### With [SDKMAN](https://sdkman.io/install)
install GRAALVM
```shell script
sdk install java x.y.z-grl
```
install QUARKUS CLI 
```shell script
sdk install quarkus 
```

### With [HOMEBREW](https://brew.sh/index_it)
install GRAALVM
```shell script
brew install --cask graalvm/tap/graalvm-ce-java17 
```
install QUARKUS CLI 
```shell script 
brew install quarkusio/tap/quarkus 
```

## Quarkus CLI
all operations of the Quarkus CLI have an equivalent maven for details [see](https://quarkus.io/guides/maven-tooling)

### Create Project
With default configuration
```shell script 
quarkus create
 ``` 
Or Create a project with groupId=com.foo, artifactId=bar, and version=1.0
 ```shell script 
quarkus create app com.foo:bar:1.0
 ``` 

Alternatively, you can also create a project from the [configurator](https://code.quarkus.io/) page.\
On this page you can view the list of all Quarkus modules

### How To Build
build a project
```shell script 
quarkus build
 ``` 
or  build with a [profile](https://quarkus.io/guides/config-reference#profiles)
 ```shell script 
quarkus build -Dquarkus.profile=profile-name-here
 ``` 

### Run
run quarkus in development mode
```shell script 
quarkus dev
``` 
Dev mode enables:
* live reload (reload on changes)
* DEVUI (configuration,status,ecc page /q/dev)
* [Swagger](https://quarkus.io/guides/openapi-swaggerui) if you import smallrye-openapi

## Extensions
### Manage project extensions
get all extensions in project
```shell script 
quarkus extension
``` 

add extension to project
```shell script 
quarkus extension add 'hibernate-validator'
```  
remove extension to project
```shell script 
quarkus ext rm kubernetes
``` 

### Search installable extension
```shell script 
quarkus ext list --concise -i -s jdbc
```  
---
### How To Build Native Executable
#### Requisite

On Rpm-based Linux Distributions
```shell script 
sudo dnf install gcc glibc-devel zlib-devel libstdc++-static
``` 
On Debian-based Linux Distributions
```shell script 
sudo apt-get install build-essential libz-dev zlib1g-dev
``` 
On Mac
```shell script 
xcode-select --install
``` 

#### build a native Executable
install graal tool
```shell script 
${GRAALVM_HOME}/bin/gu install native-image
``` 
build
```shell script 
quarkus build --native
``` 
build for docker
```shell script 
quarkus build --native -Dquarkus.native.container-build=true -Dquarkus.native.container-runtime=docker
``` 

---
#### presentation


---
## Useful Urls

* [Graal VM](https://www.graalvm.org/22.0/docs/getting-started/)
* [Get Started](https://quarkus.io/get-started/)
* [Official Guides](https://quarkus.io/guides/)
* [Quarkus Project  Creator](https://code.quarkus.io/)
* [Editor Plugins](https://quarkus.io/guides/ide-tooling)

